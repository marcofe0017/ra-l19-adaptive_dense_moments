function Lm = computeLmvcMatrix(m,chi,p,q,paug,qaug,theta_i_size,l,a)

    % Initialize the interaction matrices 
    Lm_vc   = zeros(theta_i_size, l/2);
    Lm_wc   = zeros(theta_i_size, l/2);
    Lm      = zeros(theta_i_size, l);
    Lwpm_vc   = zeros(theta_i_size, l/2);
    Lwpm_wc   = zeros(theta_i_size, l/2);

    % Extract chi information
    A = chi(1);
    B = chi(2);
    C = chi(3); 
    
    % Reshape moments as a matrix 
    mmat = reshape(m,paug+1,qaug+1);

    for i = 0 : p
        for j = 0 : q

            % Define indexes
            i_     = i + 1;
            j_     = j + 1;
            iprev_ = i_ - 1;
            jprev_ = j_ - 1;
            inext_ = i_ + 1;
            jnext_ = j_ + 1;

            % Here the index idx is computed by accessing the mat
            % column-wise
            idx      = (p +1) * j + i + 1;
   
            % subscript notation:
            % i: current x-order
            % j: current y-order
            % p: previous order
            % n: next order
            m_ij = mmat(i_,j_);
            if iprev_ > 0,        m_pj = mmat(iprev_,j_); else m_pj = 0; end
            if inext_ <= (paug+1), m_nj = mmat(inext_,j_); else m_nj = 0; end
            if jprev_ > 0,        m_ip = mmat(i_,jprev_); else m_ip = 0; end
            if jnext_ <= (qaug+1), m_in = mmat(i_,jnext_); else m_in = 0; end
            if iprev_ > 0 && jnext_ <= (qaug+1)
                m_pn = mmat(iprev_,jnext_);
            else
                m_pn = 0;
            end
            if jprev_ > 0 && inext_ <= (paug+1)
                m_np = mmat(inext_,jprev_);
            else
                m_np = 0;
            end
            
            % Fill Lmij - unweighted
            Lm_vc(idx,1) = - A * (i + 1) * m_ij ...
                             - B *       i * m_pn ...
                             - C *       i * m_pj;
            Lm_vc(idx,2) = - A *       j * m_np ...
                             - B * (j + 1) * m_ij ...
                             - C *       j * m_ip;
            Lm_vc(idx,3) =   A * (i + j + 3) * m_nj + ...
                               B * (i + j + 3) * m_in + ...
                               C * (i + j + 2) * m_ij;
            
            
            Lm_wc(idx,1) =             j * m_ip ...
                   + (i + j + 3) * m_in;
            Lm_wc(idx,2) = -           i * m_pj ...
                   - (i + j + 3) * m_nj;
            Lm_wc(idx,3) =             i * m_pn ...
                   -           j * m_np;


            
             % Fill Lmij - weighted  
            mp1q0 = m_nj;
            mp0q1 = m_in;

            if ((i_+1) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp1q1 = mmat(i_+1,j_+1); else mp1q1 = 0; end;
            if ((i_+2) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp2q0 = mmat(i_+2,j_+0); else mp2q0 = 0; end;
            if ((i_+0) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp0q2 = mmat(i_+0,j_+2); else mp0q2 = 0; end;

            if ((i_+3) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp3q0 = mmat(i_+3,j_+0); else mp3q0 = 0; end;
            if ((i_+4) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp4q0 = mmat(i_+4,j_+0); else mp4q0 = 0; end;
            if ((i_+5) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp5q0 = mmat(i_+5,j_+0); else mp5q0 = 0; end;

            if ((i_+2) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp2q1 = mmat(i_+2,j_+1); else mp2q1 = 0; end;
            if ((i_+3) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp3q1 = mmat(i_+3,j_+1); else mp3q1 = 0; end;
            if ((i_+4) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp4q1 = mmat(i_+4,j_+1); else mp4q1 = 0; end;

            if ((i_+1) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp1q2 = mmat(i_+1,j_+2); else mp1q2 = 0; end;
            if ((i_+2) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp2q2 = mmat(i_+2,j_+2); else mp2q2 = 0; end;
            if ((i_+3) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp3q2 = mmat(i_+3,j_+2); else mp3q2 = 0; end;

            if ((i_+0) <= (paug+1) && (j_+ 3) <= (qaug+1)), mp0q3 = mmat(i_+0,j_+3); else mp0q3 = 0; end;
            if ((i_+1) <= (paug+1) && (j_+ 3) <= (qaug+1)), mp1q3 = mmat(i_+1,j_+3); else mp1q3 = 0; end;
            if ((i_+2) <= (paug+1) && (j_+ 3) <= (qaug+1)), mp2q3 = mmat(i_+2,j_+3); else mp2q3 = 0; end;

            if ((i_+0) <= (paug+1) && (j_+ 4) <= (qaug+1)), mp0q4 = mmat(i_+0,j_+4); else mp0q4 = 0; end;
            if ((i_+1) <= (paug+1) && (j_+ 4) <= (qaug+1)), mp1q4 = mmat(i_+1,j_+4); else mp1q4 = 0; end;

            if ((i_+0) <= (paug+1) && (j_+ 5) <= (qaug+1)), mp0q5 = mmat(i_+0,j_+5); else mp0q5 = 0; end;

            
            Lwpm_vc(idx,1) = A*(mp4q0 + mp2q2) + ...
                             B*(mp3q1 + mp1q3) + ...
                             C*(mp3q0 + mp1q2);
            Lwpm_vc(idx,2) = A*(mp3q1 + mp1q3) + ...
                             B*(mp0q4 + mp2q2) + ...
                             C*(mp0q3 + mp2q1);
            Lwpm_vc(idx,3) = -A*(mp5q0 + 2*mp3q2 + mp1q4) ...
                             -B*(mp4q1 + 2*mp2q3 + mp0q5) ...
                             -C*(mp4q0 + 2*mp2q2 + mp0q4);

            Lwpm_wc(idx,1) = -(mp0q3 + mp2q1 + mp4q1 + 2*mp2q3 + mp0q5);
            Lwpm_wc(idx,2) = (mp3q0 + mp1q2 + mp5q0 +2*mp3q2 + mp1q4);
            Lwpm_wc(idx,3) = 0;
            
            % Build the final interaction matrix
            t1 = (mp3q1 + mp1q3);
            t2 = 4 * a;
            t3 = (j + i);
            Lm_vc(idx,:) = [-(B * m_pn + C * m_pj) * i - A * (i + 1) * m_ij + t2 * (A * (mp4q0 + mp2q2) + B * t1 + C * (mp3q0 + mp1q2)) -(A * m_np + C * m_ip) * j - B * (j + 1) * m_ij + t2 * (A * t1 + B * (mp0q4 + mp2q2) + C * (mp0q3 + mp2q1)) (A * m_nj + B * m_in) * (3 + t3) + C * (2 + t3) * m_ij - t2 * (A * (mp5q0 + 2 * mp3q2 + mp1q4) + B * (mp4q1 + 2 * mp2q3 + mp0q5) + C * (mp4q0 + 2 * mp2q2 + mp0q4));];
            Lm_wc(idx,:) = (Lm_wc(idx,:) + 4*a*Lwpm_wc(idx,:));

        end
    end
    
    % Collect in Lm matrix
    Lm(:,1:3) = Lm_vc;
    Lm(:,4:6) = Lm_wc;
    
end