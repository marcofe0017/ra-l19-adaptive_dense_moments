function VREPwriter18v2(block)
%MSFUNTMPL_BASIC A Template for a Level-2 MATLAB S-Function
%   The MATLAB S-function is written as a MATLAB function with the
%   same name as the S-function. Replace 'msfuntmpl_basic' with the 
%   name of your S-function.
%
%   It should be noted that the MATLAB S-function is very similar
%   to Level-2 C-Mex S-functions. You should be able to get more
%   information for each of the block methods by referring to the
%   documentation for C-Mex S-functions.
%
%   Copyright 2003-2010 The MathWorks, Inc.

%%
%% The setup method is used to set up the basic attributes of the
%% S-function such as ports, parameters, etc. Do not add any other
%% calls to the main body of the function.
%%
setup(block);

%endfunction

%% Function: setup ===================================================
%% Abstract:
%%   Set up the basic characteristics of the S-function block such as:
%%   - Input ports
%%   - Output ports
%%   - Dialog parameters
%%   - Options
%%
%%   Required         : Yes
%%   C-Mex counterpart: mdlInitializeSizes
%%
function setup(block)

% Register number of ports
block.NumInputPorts  = 5;
block.NumOutputPorts = 1;

% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

% Override input port properties

% camera handle
block.InputPort(1).Dimensions        = 1;
block.InputPort(1).DatatypeID        = 6; % int32
block.InputPort(1).Complexity        = 'Real';
block.InputPort(1).DirectFeedthrough = true;
block.InputPort(1).SamplingMode      = 0;

% marker handle
block.InputPort(2).Dimensions        = 1;
block.InputPort(2).DatatypeID        = 6; % int32
block.InputPort(2).Complexity        = 'Real';
block.InputPort(2).DirectFeedthrough = true;
block.InputPort(2).SamplingMode      = 0;

% Nominal camera-marker transformation
block.InputPort(3).Dimensions  = [4 4];
block.InputPort(3).DatatypeID  = 0;  % double
block.InputPort(3).Complexity  = 'Real';
block.InputPort(3).DirectFeedthrough = true;
block.InputPort(3).SamplingMode      = 0;

% Camera position
block.InputPort(4).Dimensions  = [3 1];
block.InputPort(4).DatatypeID  = 0;  % double
block.InputPort(4).Complexity  = 'Real';
block.InputPort(4).DirectFeedthrough = true;
block.InputPort(4).SamplingMode      = 0;

% Camera orientation as Euler angles
block.InputPort(5).Dimensions  = [3 1];
block.InputPort(5).DatatypeID  = 0;  % double
block.InputPort(5).Complexity  = 'Real';
block.InputPort(5).DirectFeedthrough = true;
block.InputPort(5).SamplingMode      = 0;

% start flag in output
block.OutputPort(1).Dimensions        = 1;
block.OutputPort(1).DatatypeID        = 8; % boolean
block.OutputPort(1).Complexity        = 'Real';
block.OutputPort(1).SamplingMode      = 0;

% Register parameters
block.NumDialogPrms     = 0;

% Register sample times
%  [0 offset]            : Continuous sample time
%  [positive_num offset] : Discrete sample time
%
%  [-1, 0]               : Inherited sample time
%  [-2, 0]               : Variable sample time
block.SampleTimes = [0 0];

% Specify the block simStateCompliance. The allowed values are:
%    'UnknownSimState', < The default setting; warn and assume DefaultSimState
%    'DefaultSimState', < Same sim state as a built-in block
%    'HasNoSimState',   < No sim state
%    'CustomSimState',  < Has GetSimState and SetSimState methods
%    'DisallowSimState' < Error out when saving or restoring the model sim state
block.SimStateCompliance = 'DefaultSimState';

%% -----------------------------------------------------------------
%% The MATLAB S-function uses an internal registry for all
%% block methods. You should register all relevant methods
%% (optional and required) as illustrated below. You may choose
%% any suitable name for the methods and implement these methods
%% as local functions within the same file. See comments
%% provided for each function for more information.
%% -----------------------------------------------------------------

% block.RegBlockMethod('PostPropagationSetup',    @DoPostPropSetup);
% block.RegBlockMethod('InitializeConditions', @InitializeConditions);
block.RegBlockMethod('Start', @Start);
block.RegBlockMethod('Outputs', @Outputs);     % Required
% block.RegBlockMethod('Update', @Update);
% block.RegBlockMethod('Derivatives', @Derivatives);
block.RegBlockMethod('Terminate', @Terminate); % Required

%end setup

%%
%% PostPropagationSetup:
%%   Functionality    : Setup work areas and state variables. Can
%%                      also register run-time methods here
%%   Required         : No
%%   C-Mex counterpart: mdlSetWorkWidths
% %%
% function DoPostPropSetup(block)
% block.NumDworks = 1;
%   
%   block.Dwork(1).Name            = 'x1';
%   block.Dwork(1).Dimensions      = 1;
%   block.Dwork(1).DatatypeID      = 0;      % double
%   block.Dwork(1).Complexity      = 'Real'; % real
%   block.Dwork(1).UsedAsDiscState = true;


%%
%% InitializeConditions:
%%   Functionality    : Called at the start of simulation and if it is 
%%                      present in an enabled subsystem configured to reset 
%%                      states, it will be called when the enabled subsystem
%%                      restarts execution to reset the states.
%%   Required         : No
%%   C-MEX counterpart: mdlInitializeConditions
%%
% function InitializeConditions(block)

%end InitializeConditions


%%
%% Start:
%%   Functionality    : Called once at start of model execution. If you
%%                      have states that should be initialized once, this 
%%                      is the place to do it.
%%   Required         : No
%%   C-MEX counterpart: mdlStart
%%
function Start(block)


%end Start

%%
%% Outputs:
%%   Functionality    : Called to generate block outputs in
%%                      simulation step
%%   Required         : Yes
%%   C-MEX counterpart: mdlOutputs
%%
function Outputs(block)

    global vrep 
    global vrep_id 
    global optimizeVel 
    global idealOrientation
    global B0remoteApi
    global camera_rootH refH
    global doNextStep
    
    % Initialization
    ret_p = -1;
    ret_q = -1;
    q = zeros(4,1);
    q_wxyz = zeros(4,1);
    q(4) = 1;
    q_wxyz(1) = 1;

    % Get V-REP object handles
%     cameraHdl = block.InputPort(1).Data;
%     markerHdl = block.InputPort(2).Data;
    cameraHdl = camera_rootH;
    markerHdl = refH;
    
    % Get the nominal camera-marker transformation
    Tcm = block.InputPort(3).Data;

    % Get commanded position 
    p = block.InputPort(4).Data;
    
    % Get commanded orientation
    o = block.InputPort(5).Data;

    if ~B0remoteApi
        
%         vrep.simxPauseCommunication(vrep_id,1);

        % Set the orientation in V-REP (if ideal orientation or generated by dense IBVS)
        if idealOrientation
            pm = Tcm(1:3,4);
            pm = pm/norm(pm);
            n = Tcm(1:3,3);
            z_axis = pm;
            x_axis = cross(z_axis,n);x_axis = x_axis/norm(x_axis);
            y_axis = cross(z_axis,x_axis);y_axis = y_axis/norm(y_axis);

            R = [x_axis, y_axis, z_axis];
            if(sum(isnan(R)) == 0)  
                q_wxyz = rotm2quat(R);
            end
            q(4) = q_wxyz(1);
            q(1:3) = q_wxyz(2:4);
            q(3) = 0;
            q = q/norm(q);

            ret_q = vrep.simxSetObjectQuaternion(vrep_id,cameraHdl,cameraHdl,q,vrep.simx_opmode_blocking);           
    %         ret_q = vrep.simxSetObjectQuaternion(vrep_id,cameraHdl,cameraHdl,q,vrep.simx_opmode_oneshot);
        else
            q_wxyz = eul2quat(o','XYZ');
            q = zeros(4,1);
            q(4) = q_wxyz(1);
            q(1:3) = q_wxyz(2:4);
            q = q/norm(q);
%             ret_q = vrep.simxSetObjectQuaternion(vrep_id,cameraHdl,cameraHdl,q,vrep.simx_opmode_oneshot);           
            ret_q = vrep.simxSetObjectOrientation(vrep_id,cameraHdl,cameraHdl,o,vrep.simx_opmode_oneshot);
        end

        % If the velocity is generated by the optimizer ...
        if optimizeVel
            ret_p = vrep.simxSetObjectPosition(vrep_id,cameraHdl,cameraHdl,p,vrep.simx_opmode_oneshot);
        else
            ret_p = vrep.simxSetObjectPosition(vrep_id,cameraHdl,cameraHdl,p,vrep.simx_opmode_oneshot);
        end

        % Enable the V-REP reader
        block.OutputPort(1).Data = true;

        % Trigger v-rep
        vrep.simxSynchronousTrigger(vrep_id);
        %After this call, the first simulation step is finished (Blocking function call)
        vrep.simxGetPingTime(vrep_id); 


%         vrep.simxPauseCommunication(vrep_id,0);

    else
        
        % Build the Camera Rotation Matrix
        Rx = [1,         0,          0;
              0, cos(o(1)), -sin(o(1));
              0, sin(o(1)),  cos(o(1));];
        Ry = [ cos(o(2)), 0, sin(o(2));
                       0, 1,         0;
              -sin(o(2)), 0, cos(o(2));];
        Rz = [cos(o(3)), -sin(o(3)), 0;
              sin(o(3)),  cos(o(3)), 0;
                      0,          0, 1;];
        R  = Rx * Ry * Rz;
        
        % Build Camera Transformation Matrix
        T = eye(3,4);
        T(1:3,4) = p;
        T(1:3,1:3) = R;
        Tvec = reshape(T',1,12);
        
        % Publish camera motion in V-REP
        ret = vrep.simxSetObjectMatrix(cameraHdl,cameraHdl,Tvec,vrep.simxDefaultPublisher());
%         vrep.simxSetObjectPosition(cameraHdl,cameraHdl,p,vrep.simxDefaultPublisher());
%         vrep.simxSetObjectOrientation(cameraHdl,cameraHdl,o,vrep.simxDefaultPublisher());
        
        % Enable the V-REP reader
        block.OutputPort(1).Data = true;
        
%         % Trigger V-REP
%         vrep.simxSynchronousTrigger();
% 
        % Spin once all the B0-callbacks
        vrep.simxSpinOnce();

        if doNextStep
            doNextStep=false;
            vrep.simxSynchronousTrigger();
        end

    end


%end Outputs

%%
%% Update:
%%   Functionality    : Called to update discrete states
%%                      during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlUpdate
%%
% function Update(block)
% 
% block.Dwork(1).Data = block.InputPort(1).Data;

%end Update

%%
%% Derivatives:
%%   Functionality    : Called to update derivatives of
%%                      continuous states during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlDerivatives
%%
% function Derivatives(block)

%end Derivatives

%%
%% Terminate:
%%   Functionality    : Called at the end of simulation for cleanup
%%   Required         : Yes
%%   C-MEX counterpart: mdlTerminate
%%
function Terminate(block)

%end Terminate

