global vrep vrep_id
global B0remoteApi

%q_init = [-0.0006;-0.0102;-0.1974;0.9803];
%vrep.simxSetObjectOrientation(vrep_id,camera_rootH,camera_rootH,q_init,vrep.simx_opmode_blocking);

if ~B0remoteApi
    vrep.simxStopSimulation(vrep_id,vrep.simx_opmode_oneshot_wait);
    vrep.simxFinish(-1);
else
    vrep.simxStopSimulation(vrep.simxDefaultPublisher());
    vrep.delete();
end

% Store saved data in the folder
save(strcat(resFolderName,'results_',optTypeName,'.mat'),'z_error','xi_error',...
    'ro','e_d','e_n','chi_comparison','mw1_Img','mw2_Img','mw3_Img','wImg');