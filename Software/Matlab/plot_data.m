close all;
clear all;
clc;

% Flags
vFlag = true;
tFlag = false;
vtFlag = true;

% Select the folder from which load the data to plot
% optimizationType denotes the type of optimization to be performed
% 0: only velocity optimization
% 1: only theta optimization
% 2: theta and velocity optimization
% 3: no optimization
optimizationType = 2;

simulationNum = 0;
resFolderName = strcat('./sim',int2str(simulationNum),'_data/');
switch optimizationType
    case 0
        optTypeName = 'vOpt';
    case 1
        optTypeName = 'thetaOpt';
    case 2
        optTypeName = 'v-thetaOpt';
    case 3
        optTypeName = 'noOpt';
end
resFolderName = strcat(resFolderName,optTypeName,'/');
tOptFolderName = strcat(resFolderName,'thetaOpt','/');

% Load the dataset
% Current dataset
load(strcat(resFolderName,'results_',optTypeName,'.mat'));

% Theta-optimized dataset
if tFlag
    tOptRes = load(strcat(resFolderName,'../thetaOpt/results_thetaOpt','.mat'));
end

% Theta-optimized dataset
if vFlag
    vOptRes = load(strcat(resFolderName,'../vOpt/results_vOpt','.mat'));
end

% Theta-optimized dataset
if vtFlag
    vtOptRes = load(strcat(resFolderName,'../v-thetaOpt/results_v-thetaOpt','.mat'));
end

%% rho
% Plot determinant ro of Omega*OmegaT
h = figure;
grid on; box on; hold on;
set(gca,'gridlinestyle','--'); hold on;
title('Determinant \rho'); hold on;
plot(ro,'LineWidth',2);hold on;
xlabel('Time [s]');
ylabel('\rho');

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/figures/','rho');
saveCropPdf(h,name);

%% e_d / e_n
% Plot e_d and e_n
h = figure;
grid on; box on; hold on;
title('e_d'); hold on;
plot(e_d,'LineWidth',2);hold on;
xlabel('Time [s]');
ylabel('e_d [m]');

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/figures/','e_d');
saveCropPdf(h,name);

h = figure;
grid on; box on; hold on;
title('e_n'); hold on;
plot(e_n,'LineWidth',2);hold on;
xlabel('Time [s]');
ylabel('e_n [deg]');

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/figures/','e_n');
saveCropPdf(h,name);

%% xi error
h = figure;
grid on; box on; hold on;
title('\xi error'); hold on;
plot(xi_error,'LineWidth',2);hold on;
xlabel('Time [s]');
ylabel('\xi');
legend('m_{w,1}','m_{w,2}','m_{w,3}');

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/figures/','xi_error');
saveCropPdf(h,name);

%% chi comparison / chi error
% chi comparison
chiNames = ['A';'B';'C'];
chiGTNames = strcat(chiNames,'_{GT}');
h = figure;
for i = 1 : 3
    legStr = {};
    subplot(3,1,i);
    grid on; box on; hold on;
    gtPlotNamei = chiGTNames(i,:);
    plotNamei   = chiNames(i,:);
    plot(chi_comparison.time,chi_comparison.Data(2*i-1,:),'LineWidth',2);hold on;legStr = [legStr;gtPlotNamei];
    plot(chi_comparison.time,chi_comparison.Data(2*i,:),'LineWidth',2);hold on;legStr = [legStr;plotNamei];
    xlabel('Time [s]');
    ylabel(strcat('\chi_',chiNames(i)));
    legend(legStr);
end

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/figures/','chi_comparison');
saveCropPdf(h,name);

%z error
h = figure;
grid on; box on; hold on;
title('z error'); hold on;
plot(z_error,'LineWidth',2);hold on;
xlabel('Time [s]');
ylabel('z');
legend('z_{A}','z_{B}','z_{C}');

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/figures/','z_error');
saveCropPdf(h,name);

%% COMPARISONS
% Rho
h = figure;
legStr = {};
grid on; box on; hold on;
set(gca,'gridlinestyle','--'); hold on;
title('Determinant \rho'); hold on;
if vtFlag plot(vtOptRes.ro,'LineWidth',2);hold on;legStr = [legStr;'v-\theta Opt.']; end
if tFlag plot(tOptRes.ro,'LineWidth',2);hold on;legStr = [legStr;'\theta Opt.']; end
if vFlag plot(vOptRes.ro,'LineWidth',2);hold on;legStr = [legStr;'v Opt.']; end
xlabel('Time [s]');
ylabel('\rho');
legend(legStr);

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/../comparison/','rho');
saveCropPdf(h,name);

% e_d / e_n
h = figure;
legStr = {};
grid on; box on; hold on;
title('e_d'); hold on;
if vtFlag plot(vtOptRes.e_d,'LineWidth',2);hold on;legStr = [legStr;'v-\theta Opt.']; end
if tFlag plot(tOptRes.e_d,'LineWidth',2);hold on;legStr = [legStr;'\theta Opt.'];end
if vFlag plot(vOptRes.e_d,'LineWidth',2);hold on;legStr = [legStr;'v Opt.'];end
xlabel('Time [s]');
ylabel('e_d [m]');
legend(legStr);

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/../comparison/','e_d');
saveCropPdf(h,name);

h = figure;
legStr = {};
grid on; box on; hold on;
title('e_n'); hold on;
if vtFlag plot(vtOptRes.e_n,'LineWidth',2);hold on;legStr = [legStr;'v-\theta Opt.']; end
if tFlag plot(tOptRes.e_n,'LineWidth',2);hold on;legStr = [legStr;'\theta Opt.']; end
if vFlag plot(vOptRes.e_n,'LineWidth',2);hold on;legStr = [legStr;'v Opt.']; end
xlabel('Time [s]');
ylabel('e_n [deg]');
legend(legStr);

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/../comparison/','e_n');
saveCropPdf(h,name);

% xi error
h = figure;
grid on; box on; hold on;
title('\xi error'); hold on;
if vtFlag 
    plot(vtOptRes.xi_error,'LineWidth',2);hold on; 
    legStr = [legStr; 'v-\theta m_{w,1}';'v-\theta m_{w,2}';'v-\theta m_{w,3}';]; 
end
if tFlag 
    plot(tOptRes.xi_error,'LineWidth',2,'LineStyle',':');hold on; 
    legStr = [legStr; '\theta m_{w,1}';'\theta m_{w,2}';'\theta m_{w,3}';]; 
end
if vFlag 
    plot(vOptRes.xi_error,'LineWidth',2,'LineStyle','-.');hold on; 
    legStr = [legStr; 'v m_{w,1}';'v m_{w,2}';'v m_{w,3}';]; 
end
xlabel('Time [s]');
ylabel('\xi');
legend(legStr);

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/../comparison/','xi_error');
saveCropPdf(h,name);


% chi comparison
chiNames = ['A';'B';'C'];
chiGTNames = strcat(chiNames,'_{GT}');
h = figure;
for i = 1 : 3
    legStr = {};
    subplot(3,1,i);
    grid on; box on; hold on;
    gtPlotNamei = chiGTNames(i,:);
    plotNamei   = chiNames(i,:);
    if vtFlag
        plot(vtOptRes.chi_comparison.time,vtOptRes.chi_comparison.Data(2*i-1,:),'LineWidth',2);hold on;
        plot(vtOptRes.chi_comparison.time,vtOptRes.chi_comparison.Data(2*i,:),'LineWidth',2);hold on;
        legStr = [legStr;strcat(plotNamei,' v-\theta Opt.');];
        legStr = [legStr;strcat(gtPlotNamei,' v-\theta Opt.');];
    end
    if tFlag
        plot(tOptRes.chi_comparison.time,tOptRes.chi_comparison.Data(2*i-1,:),'LineWidth',2,'LineStyle',':');hold on;
        plot(tOptRes.chi_comparison.time,tOptRes.chi_comparison.Data(2*i,:),'LineWidth',2,'LineStyle',':');hold on;
        legStr = [legStr;strcat(plotNamei,' \theta Opt.');];
        legStr = [legStr;strcat(gtPlotNamei,' \theta Opt.');];
    end
    if vFlag
        plot(vOptRes.chi_comparison.time,vOptRes.chi_comparison.Data(2*i-1,:),'LineWidth',2,'LineStyle','-.');hold on;
        plot(vOptRes.chi_comparison.time,vOptRes.chi_comparison.Data(2*i,:),'LineWidth',2,'LineStyle','-.');hold on;
        legStr = [legStr;strcat(plotNamei,' v Opt.');];
        legStr = [legStr;strcat(gtPlotNamei,' v Opt.');];
    end
    xlabel('Time [s]');
    ylabel(strcat('\chi_',chiNames(i)));
    legend(legStr);
end

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/../comparison/','chi_comparison');
saveCropPdf(h,name);

%z error
h = figure;
legStr = {};
grid on; box on; hold on;
title('z error'); hold on;
if vtFlag
    plot(vtOptRes.z_error,'LineWidth',2);hold on;
    legStr = [legStr;'z_A v-\theta Opt.';'z_B v-\theta Opt.';'z_C v-\theta Opt.';];
end
if tFlag
    plot(tOptRes.z_error,'LineWidth',2,'LineStyle',':');hold on;
    legStr = [legStr;'z_A \theta Opt.';'z_B \theta Opt.';'z_C \theta Opt.';];
end
if vFlag
    plot(vOptRes.z_error,'LineWidth',2,'LineStyle','-.');hold on;
    legStr = [legStr;'z_A v Opt.';'z_B v Opt.';'z_C v Opt.';];
end
xlabel('Time [s]');
ylabel('z');
legend(legStr);

% Save figure as pdf and png (cropped)
name = strcat(resFolderName,'/../comparison/','z_error');
saveCropPdf(h,name);
