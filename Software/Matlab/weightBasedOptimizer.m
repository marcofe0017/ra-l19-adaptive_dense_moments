function [vdot, tdot, ro] = weightBasedOptimizer(Omega,dLm_chidv,Lm_chi,theta,v,ki_vdes,ki_tdes,kv1,kv2,kt1,kt2,n_m,theta_i_size,tol)

% Identity matrix of size theta_i_size x theta_i_size
It = eye(theta_i_size,theta_i_size);

% Identity matrix of size 3 x 3
I3 = eye(3,3);

% Jv
Jv = zeros(1,3);

% Jtheta
Jtheta = zeros(n_m,theta_i_size);

% vdot
vdot = zeros(3,1);

% thetadot
tdot = zeros(theta_i_size,n_m);

% Compute the observability matrix with determinant
OmOmT = Omega * Omega';
ro = det(OmOmT);

% Compute adjoint matrix of Omega*Omega'
adjOmOmT = zeros(n_m,n_m);
if(ro>tol)
    adjOmOmT = ro*inv(OmOmT);
end

% Compute the Hermitian product of theta matrix
TT = theta * theta';

%% Acceleration input
for i = 1 : size(Jv,2)
    
    % Compute the derivative of Omega*OmegaT wrt vi
    Mv = dLm_chidv(:,:,i)' * TT * Lm_chi;
    dOmOmTdvi = Mv + Mv';

    % Compute the Jv term
    Jv(i) = trace(adjOmOmT * dOmOmTdvi);
end

% Normalize the desired vdot input
if(norm(Jv) > tol)
    Jv = kv2 * Jv/norm(Jv);
else
    Jv;
end

if (norm(v) > tol)
    % Compute the Null-Space-Projection (NSP) for the desired vdot input
    NSPJv = (I3 - v*v'/(v'*v)) * Jv';

    % Compute the vdot inputs
    ki_v = 0.5*(v'*v);
    vdot = kv1 * (v/(v'*v)) * (ki_vdes - ki_v) + NSPJv;
else
    vdot = zeros(3,1);
end

%% thetadot input
for i = 1 : n_m

    for j = 1 : theta_i_size
        
        % Compute the derivative of Omega*OmegaT wrt vi
        dThetadti = zeros(theta_i_size,n_m);
        dThetadti(j,i) = 1;
        Mtheta = Lm_chi' * dThetadti * theta' * Lm_chi;
        dOmOmdti = Mtheta + Mtheta';
        
        % Compute the Jtheta terms 
        Jtheta(i,j) = trace(adjOmOmT * dOmOmdti);
        
    end
    
    % Normalize the desired tdot input
    if(norm(Jtheta(i,:))>tol)
        Jtheta(i,:) = kt2 * Jtheta(i,:)/norm(Jtheta(i,:));
    else
        Jtheta(i,:);
    end
    
    % Compute the Null-Space-Projection (NSP) for the desired tdot input
    NSPJti = (It - theta(:,i)*theta(:,i)'/(theta(:,i)'*theta(:,i))) * Jtheta(i,:)';

    % Compute the tdot inputs
    ki_t = 0.5*(theta(:,i)'*theta(:,i));
    tdot(:,i) = kt1 * (theta(:,i) / (theta(:,i)'*theta(:,i))) * (ki_tdes(i)- ki_t) + NSPJti;
    

end

