%% Plot mw_Imgs 
close all;
clc;

global az;
global el;
global img_idx;
global show;

plot3d = false;
imgArray = true;

% Select the folder from which load the data to plot
% optimizationType denotes the type of optimization to be performed
% 0: only velocity optimization
% 1: only theta optimization
% 2: theta and velocity optimization
% 3: no optimization
optimizationType = 2;

simulationNum = 0;
resFolderName = strcat('./sim',int2str(simulationNum),'_data/');
switch optimizationType
    case 0
        optTypeName = 'vOpt';
    case 1
        optTypeName = 'thetaOpt';
    case 2
        optTypeName = 'v-thetaOpt';
    case 3
        optTypeName = 'noOpt';
end
resFolderName = strcat(resFolderName,optTypeName,'/');

% Load the dataset
% Current dataset
load(strcat(resFolderName,'results_',optTypeName,'.mat'));



t = mw1_Img.Time;

h1 = figure;

az = 10;
el = 10;
set(h1,'KeyPressFcn',@changeViewPoint);

zlim = 1e-5;
off = 4;


show = true;
img_idx = 1;

x = [0:off:320-off];
y = [0:off:240-off];
[X,Y] = meshgrid(x,y);

if plot3d
    for i = 1:5:size(t)
%     while show
% 
%         if img_idx > size(t,1)
%             img_idx = size(t,1);
%         elseif img_idx < 1
%             img_idx = 1
%         end
% 
%         i = img_idx;
        
        t(i)

        figure(h1);
        clf;
        subplot(2,3,1);
        grid on; hold on;
        title('m_{\theta,1}');hold on;
        mesh(X,Y,mw1_Img.Data(1:off:end,1:off:end,i)); 
        view([az el]);
        axis([0 320 0 240 -zlim zlim]);
        shading flat

        subplot(2,3,2);
        grid on; hold on;
        title('m_{\theta,2}');hold on;
        mesh(X,Y,mw2_Img.Data(1:off:end,1:off:end,i)); 
        view([az el]);
        axis([0 320 0 240 -zlim zlim]);
        shading flat

        subplot(2,3,3);
        grid on; hold on;
        title('m_{\theta,3}');hold on;
        mesh(X,Y,mw3_Img.Data(1:off:end,1:off:end,i)); 
        view([az el]);
        axis([0 320 0 240 -zlim zlim]);
        shading flat


        subplot(2,3,4:6);
        grid on; hold on;
        titleStr = strcat('m_{\theta,3} - ',num2str(t(i)));
        title(titleStr);hold on;
        mesh(X,Y,wImg.Data(1:off:end,1:off:end,i)); 
        view([az el]);
        axis([0 320 0 240 0 255]);
        shading flat

        drawnow;
    end
end

if imgArray
    figure;
    while show
        if img_idx > size(t,1)
            img_idx = size(t,1);
        elseif img_idx < 1
            img_idx = 1
        end
        istr = num2str(t(img_idx));
        title(istr);hold on;
        imshow(wImg.Data(1:end,1:end,img_idx));hold on;
        drawnow;
    end
end

function changeViewPoint(~,evnt)
    global az;
    global el;
    global show;
    global img_idx;
    
    fprintf('key pressed: %s\n',evnt.Key);
    
    if strcmpi(evnt.Key,'leftarrow')
%         az = az - 5;
        img_idx = img_idx - 5;
    elseif strcmpi(evnt.Key,'rightarrow')
%         az = az + 5;
        img_idx = img_idx + 5;
    elseif strcmpi(evnt.Key,'downarrow')
        el = el + 5;
    elseif strcmpi(evnt.Key,'uparrow')
        el = el - 5;
        show = false;
    end  
end