global vrep vrep_id timeLog dynEnabled
global wType
global enableVision
global B0remoteApi
global cameraH camera_rootH refH
global doNextStep
global start

%% Enable V-REP remote APIs (B0-based or legacy)
B0remoteApi = true;
doNextStep = true;
start = false;

if ~B0remoteApi
    vrep=remApi('remoteApi');
    vrep.simxFinish(-1); % just in case, close all opened connections
    vrep_id=vrep.simxStart('127.0.0.1',19997,true,true,2000,5);
    if (vrep_id>-1)
        disp('Connected to remote API server');
    else
        disp('Could not connect to remote API server');
        disp('stop simulation')
        vrep.delete();
        set_param(gcs, 'SimulationCommand', 'stop')
    end
    
    % Get object handles
    [ret,cameraH]      = vrep.simxGetObjectHandle(vrep_id,'camera',vrep.simx_opmode_blocking);
    [ret,camera_rootH] = vrep.simxGetObjectHandle(vrep_id,'camera_root',vrep.simx_opmode_blocking);
    [ret2,refH]        = vrep.simxGetObjectHandle(vrep_id,'ref_point',vrep.simx_opmode_blocking);
%     [ret3,rollH]       = vrep.simxGetObjectHandle(vrep_id,'roll',vrep.simx_opmode_blocking);
%     [ret4,pitchH]      = vrep.simxGetObjectHandle(vrep_id,'pitch',vrep.simx_opmode_blocking);
%     [ret5,yawH]        = vrep.simxGetObjectHandle(vrep_id,'yaw',vrep.simx_opmode_blocking);
%     [ret6,cub1H]       = vrep.simxGetObjectHandle(vrep_id,'Cuboid',vrep.simx_opmode_blocking);
%     [ret7,cub2H]       = vrep.simxGetObjectHandle(vrep_id,'Cuboid2',vrep.simx_opmode_blocking);
%     [ret8,cub3H]       = vrep.simxGetObjectHandle(vrep_id,'Cuboid3',vrep.simx_opmode_blocking);
%     [ret2,planeH] = vrep.simxGetObjectHandle(vrep_id,'Plane',vrep.simx_opmode_blocking);

    [ret9 , qcm] = vrep.simxGetObjectQuaternion(vrep_id,refH,camera_rootH,vrep.simx_opmode_blocking);
    [ret10, pcm] = vrep.simxGetObjectPosition(vrep_id,refH,camera_rootH,vrep.simx_opmode_blocking);

    qcm_ = qcm;
    qcm_(1) = qcm(4);
    qcm_(2:4) = qcm(1:3);
    Rcm = quat2rotm(qcm_);
    n = Rcm(:,3);
    z_axis = pcm/norm(pcm);
    x_axis = cross(z_axis,n);x_axis = x_axis/norm(x_axis);
    y_axis = cross(z_axis,x_axis);y_axis = y_axis/norm(y_axis);
    Rdes = [x_axis',y_axis',z_axis'];
    qdes_ = rotm2quat(Rdes);
    qdes = qdes_;
    qdes(1:3) = qdes_(2:4);
    qdes(4)   = qdes_(1);

    abg0 = zeros(3,1);
    % Set initial orientation on camera
    % [ret,q_init0] = vrep.simxGetObjectQuaternion(vrep_id,camera_rootH,-1,vrep.simx_opmode_blocking);
    % q_init = [-0.0006;-0.0102;-0.1974;0.9803];
    %vrep.simxSetObjectQuaternion(vrep_id,camera_rootH,camera_rootH,qdes,vrep.simx_opmode_blocking);
    if dynEnabled    
        % Enable dynamics in V-REP
        vrep.simxSetBooleanParameter(vrep_id,vrep.sim_boolparam_dynamics_handling_enabled,true,vrep.simx_opmode_blocking);

        % Set cuboid shapes dynamic
        vrep.simxSetObjectIntParameter(vrep_id,cub1H,vrep.sim_shapeintparam_static,0,vrep.simx_opmode_blocking);
        vrep.simxSetObjectIntParameter(vrep_id,cub2H,vrep.sim_shapeintparam_static,0,vrep.simx_opmode_blocking);
        vrep.simxSetObjectIntParameter(vrep_id,cub3H,vrep.sim_shapeintparam_static,0,vrep.simx_opmode_blocking);
    else
        % Disable dynamics in V-REP
        vrep.simxSetBooleanParameter(vrep_id,vrep.sim_boolparam_dynamics_handling_enabled,false,vrep.simx_opmode_blocking);
    end

    %% Get information from camera and build calibration matrix
    if ~offlineLog
        %% Start simulation
        vrep.simxStartSimulation(vrep_id,vrep.simx_opmode_oneshot_wait);
        vrep.simxSynchronous(vrep_id,true);

        vrep.simxSetObjectPosition(vrep_id,camera_rootH,camera_rootH,v0*Ts,vrep.simx_opmode_oneshot);
        
        % Trigger v-rep
        vrep.simxSynchronousTrigger(vrep_id);

        %After this call, the first simulation step is finished (Blocking function call)
        vrep.simxGetPingTime(vrep_id); 


        [ret, res, image] = vrep.simxGetVisionSensorImage(vrep_id,cameraH,1,...
            vrep.simx_opmode_blocking);
        [ret, angle]      = vrep.simxGetObjectFloatParameter(vrep_id,cameraH,...
            vrep.sim_visionfloatparam_perspective_angle,vrep.simx_opmode_blocking);
        start = true;

    else
        res(1) = size(image0Log,2);
        res(2) = size(image0Log,1);
    end

else
    try
        vrep = b0RemoteApi('b0RemoteApi_matlabClient','b0RemoteApi');

        %% Get object handles
        cameraH      = vrep.simxGetObjectHandle('camera',vrep.simxServiceCall());
        camera_rootH = vrep.simxGetObjectHandle('camera_root',vrep.simxServiceCall());
        refH         = vrep.simxGetObjectHandle('ref_point',vrep.simxServiceCall());

        cameraH      = int32(cameraH{2});
        camera_rootH = int32(camera_rootH{2});
        refH         = int32(refH{2});

        % Disable dynamics in V-REP
        vrep.simxSetBoolParameter('sim.boolparam_dynamics_handling_enabled',false,vrep.simxServiceCall());

        if ~offlineLog
            %% Start simulation
            vrep.simxSynchronous(true);
            vrep.simxStartSimulation(vrep.simxServiceCall());

            % Command camera
            Tc = eye(3,4);
            pos = v0*Ts;
            Tc(1:3,4) = pos;
            Tvec = reshape(Tc',1,12);
%             vrep.simxSetObjectPosition(camera_rootH,camera_rootH,v0*Ts,vrep.simxServiceCall());
            ret = vrep.simxSetObjectMatrix(camera_rootH,camera_rootH,Tvec,vrep.simxDefaultPublisher());

            % Retrieve first camera image
            data = vrep.simxGetVisionSensorImage(cameraH,false,vrep.simxServiceCall());
            res(1) = data{2}{1};
            res(2) = data{2}{2};
            
            % Retrieve camera angle
            vrep.simxGetObjectFloatParameter(cameraH,...
                'sim.visionfloatparam_perspective_angle',vrep.simxServiceCall());

            % Trigger V-REP
            vrep.simxSynchronousTrigger();
            
            % Calls all callbacks for subscribers that have messages waiting, then returns
%             vrep.simxSpinOnce();

            
            start = true;

        else
            res(1) = size(image0Log,2);
            res(2) = size(image0Log,1);        
        end
    catch me
        vrep.simxStopSimulation(vrep.simxServiceCall());
        vrep.delete();
        rethrow(me);
    end
end
enableVision = false;

% Load array of logged images
dirimages = dir(strcat(folder,'*.jpg'));
filenames = {dirimages.name};
filenames = natsortfiles(filenames);

% imgVec = zeros(res(2),res(1),size(filenames,2)-1,'uint8');
% 
% for i = 1 : size(filenames,2)-1
%     img = imread(strcat(folder,filenames{i}));
%     if(size(img,1)==size(imgVec(:,:,i),1) && size(img,2) == size(imgVec(:,:,i),2))
%         imgVec(:,:,i) = imread(strcat(folder,filenames{i}));
%     end
% end
% 
% imgVec_t = timeseries(imgVec);
% imgVec_t.Time = timeLog;

% image.size = res;
r = double(res(2));
c = double(res(1));
if(res(1) > res(2)) 
    resMax =  res(1); 
else
    resMax = res(2); 
end
focus = double(resMax)/(2*tan(angle/2));

K = [focus,     0, res(1)*0.5;
         0, focus, res(2)*0.5;
         0,     0,          1];

%% Test the camera
% [ret, res, image] = vrep.simxGetVisionSensorImage2(vrep_id,cameraH,10,vrep.simx_opmode_blocking);
%imshow(image);
imgZero = ones(res(2),res(1))*255;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FOR ONLINE EXECUTION %%%
% %% Define the camera path (expressed in marker frame F_m)
center = [0.0;3.0;3.0];
normal = [0.0;0.0;1.0];
radius = 2.0;

% Get the pair (a,b) of perpendicular axes to n
if(normal(3) ~= 0)
    ax = [1; 0; -normal(1)/normal(3)];
elseif(normal(2) ~= 0)
    ax = [1; -normal(1)/normal(2); 0];
elseif(normal(1) ~= 0)
    ax = [-normal(3)/normal(1); 1; 0]; % maybe wrong, remember
end
by = cross(normal,ax);

% Define the circular path and its derivative
scale_z = 3.0;
pos  = repmat(center',N,1) + radius * [cos(s)' sin(s)']*[ax by]';
pos(:,3) = center(3) + sp * scale_z;
pdot = - radius * sdot * [sin(s)' cos(s)'] * [ax by]';  % 
pdot(:,3) = scale_z * spdot;

% Define the known linear velocity vector in the camera frame. Since the
vc   = zeros(size(pdot));
vc(:,1) = sdot/100;

% Define path and velocity profiles as timeseries
pos_t = timeseries(pos); pos_t.Time = t;
pdot_t = timeseries(pdot); pdot_t.Time = t;
vc_t = timeseries(vc); vc_t.Time = t;

%%% SHOULD NOT BE USED NOW %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SHOULD NOT BE USED NOW %%%
% Create the shaded image mask
% w = res(1);
% h = res(2);
% shade_size = 4;
% mask = ones(h,w,'uint8')*255;
% maskLR = mask;
% maskUD = mask;
% 
% maskLR(:,1:w/shade_size) = repmat([1:double(255/double(w/shade_size)):255],h,1);
% maskLR(:,end-w/shade_size+1:end) = repmat(fliplr([1:double(255/double(w/shade_size)):255]),h,1);
% maskUD(1:h/shade_size,:) = repmat([1:double(255/double(h/shade_size)):255]',1,w);
% maskUD(end-h/shade_size:end,:) = repmat(fliplr([0:double(255/double(h/shade_size)):255])',1,w);
% mask = min(maskLR,maskUD);
% imshow(mask);
% 
% Comment the line below if you want to apply the mask on the image
% mask = ones(res(2),res(1))*255;
% normdMask = double(mask)./255;
%%% SHOULD NOT BE USED NOW %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters of the weighting function in the photometric moments
x = (linspace(-c/2,c/2,c)/focus);X = repmat(x,r,1);
y = (linspace(-r/2,r/2,r)/focus);Y = repmat(y',1,c);
dx = (x(2)-x(1));
dy = (y(2)-y(1));
Kw = 1;
a  = 150;

if wType == 0
    wFcn = ones(r,c);
elseif wType == 1
    wFcn = Kw*exp(-a*(X.^2 + Y.^2));
elseif wType == 2
    wFcn = Kw*exp(-a*(X.^2 + Y.^2).^2);
end

% Fill the "basis" matrix to compute the photometric moments
% Compute all the photometric moments from (0,0) up to (p+1,q+1) order
mbasis = zeros(r,c,(paug+1)*(qaug+1));
for i = 0 : paug 
    for j = 0 : qaug
        idx = i*(qaug+1) + j + 1;
        basis_ij =  dx*dy * (y'.^j * x.^i);
        mbasis(:,:,idx) = basis_ij;
        basis_ij;            
    end
end
