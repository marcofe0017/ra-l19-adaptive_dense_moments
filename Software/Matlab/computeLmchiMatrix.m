function Lm_chi = computeLmchiMatrix(v,m,p,q,paug,qaug,theta_i_size,n_nm,a)

    % Initialization
    Lm_chi  = zeros(theta_i_size,n_nm);


    % Recall variable
    vc = v;

    % Reshape moments as a matrix 
    mmat = reshape(m,paug+1,qaug+1);

    for i = 0 : p
        for j = 0 : q

            % Define indexes
            i_     = i + 1;
            j_     = j + 1;
            iprev_ = i_ - 1;
            jprev_ = j_ - 1;
            inext_ = i_ + 1;
            jnext_ = j_ + 1;

            % Here the index idx is computed by accessing the mat
            % column-wise
            idx      = (p +1) * j + i + 1;
   
            % subscript notation:
            % i: current x-order
            % j: current y-order
            % p: previous order
            % n: next order
            m_ij = mmat(i_,j_);
            if iprev_ > 0,        m_pj = mmat(iprev_,j_); else m_pj = 0; end
            if inext_ <= (paug+1), m_nj = mmat(inext_,j_); else m_nj = 0; end
            if jprev_ > 0,        m_ip = mmat(i_,jprev_); else m_ip = 0; end
            if jnext_ <= (qaug+1), m_in = mmat(i_,jnext_); else m_in = 0; end
            if iprev_ > 0 && jnext_ <= (qaug+1)
                m_pn = mmat(iprev_,jnext_);
            else
                m_pn = 0;
            end
            if jprev_ > 0 && inext_ <= (paug+1)
                m_np = mmat(inext_,jprev_);
            else
                m_np = 0;
            end
            
            mp1q0 = m_nj;
            mp0q1 = m_in;

            if ((i_+1) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp1q1 = mmat(i_+1,j_+1); else mp1q1 = 0; end;
            if ((i_+2) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp2q0 = mmat(i_+2,j_+0); else mp2q0 = 0; end;
            if ((i_+0) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp0q2 = mmat(i_+0,j_+2); else mp0q2 = 0; end;

            if ((i_+3) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp3q0 = mmat(i_+3,j_+0); else mp3q0 = 0; end;
            if ((i_+4) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp4q0 = mmat(i_+4,j_+0); else mp4q0 = 0; end;
            if ((i_+5) <= (paug+1) && (j_+ 0) <= (qaug+1)), mp5q0 = mmat(i_+5,j_+0); else mp5q0 = 0; end;

            if ((i_+2) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp2q1 = mmat(i_+2,j_+1); else mp2q1 = 0; end;
            if ((i_+3) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp3q1 = mmat(i_+3,j_+1); else mp3q1 = 0; end;
            if ((i_+4) <= (paug+1) && (j_+ 1) <= (qaug+1)), mp4q1 = mmat(i_+4,j_+1); else mp4q1 = 0; end;

            if ((i_+1) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp1q2 = mmat(i_+1,j_+2); else mp1q2 = 0; end;
            if ((i_+2) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp2q2 = mmat(i_+2,j_+2); else mp2q2 = 0; end;
            if ((i_+3) <= (paug+1) && (j_+ 2) <= (qaug+1)), mp3q2 = mmat(i_+3,j_+2); else mp3q2 = 0; end;

            if ((i_+0) <= (paug+1) && (j_+ 3) <= (qaug+1)), mp0q3 = mmat(i_+0,j_+3); else mp0q3 = 0; end;
            if ((i_+1) <= (paug+1) && (j_+ 3) <= (qaug+1)), mp1q3 = mmat(i_+1,j_+3); else mp1q3 = 0; end;
            if ((i_+2) <= (paug+1) && (j_+ 3) <= (qaug+1)), mp2q3 = mmat(i_+2,j_+3); else mp2q3 = 0; end;

            if ((i_+0) <= (paug+1) && (j_+ 4) <= (qaug+1)), mp0q4 = mmat(i_+0,j_+4); else mp0q4 = 0; end;
            if ((i_+1) <= (paug+1) && (j_+ 4) <= (qaug+1)), mp1q4 = mmat(i_+1,j_+4); else mp1q4 = 0; end;

            if ((i_+0) <= (paug+1) && (j_+ 5) <= (qaug+1)), mp0q5 = mmat(i_+0,j_+5); else mp0q5 = 0; end;

            
            t1 = 4 * a;
            t2 = 2;
            t3 = (j + i);
            t4 = t1 * (mp3q1 + mp1q3);
            t5 = 3 + t3;
            Lm_chi(idx,:) = ...
                [(-(i + 1)*m_ij + t1*(mp4q0 + mp2q2))*vc(1) + (-j*m_np + t4)*vc(2) + (m_nj*t5 + t1*(-mp3q2*t2 - mp1q4 - mp5q0))*vc(3),...
                 (-i*m_pn + t4)*vc(1) + (-(j + 1)*m_ij + t1*(mp0q4 + mp2q2))*vc(2) + (m_in*t5 + t1*(-mp2q3*t2 - mp0q5 - mp4q1))*vc(3),...
                 (-i*m_pj + t1*(mp3q0 + mp1q2))*vc(1) + (-j*m_ip + t1*(mp0q3 + mp2q1))*vc(2) + ((t2 + t3)*m_ij - t1*(mp2q2*t2 + mp0q4 + mp4q0))*vc(3)];

        end
    end
end