%%
% Hai messo la vc e la wc comandata in input al process dynamics. Andrà
% messa quella, però devi assicurarti che le velocità siano correttamente
% attuate, altrimenti serve il loop con la velocità misurata (optical flow)

% init_function
close all;
clear;
clc;

% Flag to select if data are read from offline log files or online from
% V-REP simulation
global offlineLog;
global timeLog;
global wType;
global optimizeVel;
global dynEnabled;
global useObserver;
global idealOrientation;
global problemType;
global ctrlFeatType;
global useMom00;

offlineLog = false;
optimizeVel = true;
dynEnabled = false;
idealOrientation = false;
useMom00 = true;
newThetaCoeffs = false;

% optimizationType denotes the type of optimization to be performed
% 0: only velocity optimization
% 1: only theta optimization
% 2: theta and velocity optimization
% 3: no optimization
optimizationType = 2;

simulationNum = 1;
resFolderName = strcat('./sim',int2str(simulationNum),'_data/');
switch optimizationType
    case 0
        optTypeName = 'vOpt';
    case 1
        optTypeName = 'thetaOpt';
    case 2
        optTypeName = 'v-thetaOpt';
    case 3
        optTypeName = 'noOpt';
end
resFolderName = strcat(resFolderName,optTypeName,'/');

% ctrlFeatType denotes the type of features used for the controller
% 0: s = [xg;yg;alpha]
% 1: s = [mw(theta1);mw(theta2);mw(theta3)]
ctrlFeatType = 0;

% problemType denotes the type of addressed problems, among:
%   0: 'classical set' problem (s = [a xg yg])
%   1: weighted polynomial moments problem
%   2: ...
problemType = 1;

% Type of weighting function
% 0: 1 for all (x,y) 
% 1: exp^-a(x^2 + y^2)
% 2: exp^-a(x^2 + y^2)^2
wType = 2;

% Read data from files, in case of offline-logged data
folder   = 'logs/vrep_log/';
vcLog    = load(strcat(folder,'vcc.txt'));
Tk0Log   = load(strcat(folder,'Tk0.txt'));
timeLog  = load(strcat(folder,'stamps.txt'));
planeLog = load(strcat(folder,'plane.txt'));
angle    = load(strcat(folder,'perspAngle.txt'));

vcLog_t    = timeseries(vcLog(:,2:end));
Tk0Log_t   = timeseries(Tk0Log(:,2:end));
planeLog_t = timeseries(planeLog(:,2:end)); 
vcLog_t.Time  = timeLog;
Tk0Log_t.Time = timeLog;
planeLog_t.Time = timeLog;

plane0Log = planeLog_t.Data(1,:);
image0Log = imread(strcat(folder,'image0.jpg'));

% Define Sample Time and time vector
Ts = 0.05;
if ~offlineLog
    T  = 20;
else
    T = timeLog(end);
end

t = [0 : Ts : T-Ts]; % Time vector
s = 2*pi*t/(T); % path parameter
sp = t/T;
sdot = 2*pi/(T);
spdot = 1/T;
N = floor(T/Ts);

% Maximum orders of photometric moments to be taken into account in the
% measurement model and in the generation of x_m = s = theta'*m
% Given the number specified here, the considered moments are up to p + 1
% and q + 1
p = 2;
q = 2;

% Maximum order of photometric moment to measure. The dynamics of a
% generic weighted moment m_ij depends on the moments of higher orders up
% to (i + j + 5), therefore, even if we do not need to propagate, we need
% to measure them to correctly propagate the moments up to order (p + q +
% 1)
paug = p + 5;
qaug = q + 5;
msize = (paug + 1) * (qaug + 1); % size of the vector of moments to measure

% OBSOLETE DESCRIPTION: 
% be take into account in the
% measurement model (Remember that, since the IP assumption is already
% guaranteed through the weighting function, there is no need to enforce it
% through a higher number of coefficients in the polynomial moment.
% However, it is important to keep a consistent and accurate prediction of
% the propagated moments by considering high orders of propagation (see p
% and q above), while using only a subset of the moments propagated in the
% polynomial weighted moment.
% Also in this case, given the number specified here, the number of
% coefficients theta to be defined are pmeas+1 and qmeas+1 (as the constant
% term is also taken into account)

% Size of the estimating state
% The estimating state is built this way:
%
% q = [m_ij | chi]^T
%
% where
%
% m_ij, i = 0, ..., p+1; j = 0 , ... , q+1; :  measurable component of size 
% n_m = number of weighted polynomial moments 
%
% and
%
% chi = [A B C]^T: non-measurable component of size n_nm = 3
n_m  = 3;
n_nm = 3;

% Size of the input vector
% The input vector is built this way:
% u = vc: 6-D camera velocity
l = 6;

% Initialize theta coefficients
theta_i_size = (p+1)*(q+1);
theta_size = theta_i_size*n_m;

if newThetaCoeffs
    if useMom00
        theta0 = 2 * rand(theta_i_size,n_m) - 1;
    else
        theta0 = [zeros(1,n_m);2 * rand(theta_i_size-1,n_m) - 1];
    end
else
    theta0 = load('theta0.mat');
    theta0 = theta0.theta0;
end

% Normalize theta vectors
scale = ones(n_m,1).*diag([1,1,1]);
ki_tdes = zeros(n_m,1);
for i = 1 : n_m
    if useMom00
        theta0(:,i) = theta0(:,i)*scale(i,i)/sqrt(theta0(:,i)'*theta0(:,i));
        ki_tdes(i) = 0.5*(theta0(:,i)'*theta0(:,i));
    else
        theta0(2:end,i) = theta0(2:end,i)*scale(i,i)/sqrt(theta0(2:end,i)'*theta0(2:end,i));
        ki_tdes(i) = 0.5*(theta0(2:end,i)'*theta0(2:end,i));
    end
end

if newThetaCoeffs
    save('theta0');
end

thetadot = zeros(theta_i_size,n_m);

% theta_init = theta;
% save('theta_init');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SHOULD NOT BE USED NOW %%%
% For the moment, define here the array of parameters theta_p and theta_q
% theta  = ones(((p+1)+(q+1))*k,1)*0.2;
% dtheta = ones(((p+1)+(q+1))*k,1)*0.5;

% For the moment, define here the derivatives of the parameters theta
% A = [1 : 1 : k];
% f = [1 : 0.5 : 0.5*(p+1+1)]';
% 
% theta = [cos(2*pi*f*s);sin(2*pi*f*s);];
% theta = repmat(theta,k,1);
% Arep  = repmat(reshape(repmat(A,p+1+q+1,1),(p+1+q+1)*k,1),1,size(s,2));
% theta = Arep.*theta;% theta = amps.*cos(s)';
% 
% dtheta = [-sin(2*pi*f*s);cos(2*pi*f*s);];
% dtheta = repmat(dtheta,k,1);
% frep = repmat(repmat(f,2,1),k,size(s,2));
% dtheta = Arep.*dtheta.*frep*2*pi*sdot;
% 
% theta_t  = timeseries(theta');
% dtheta_t = timeseries(dtheta');
% theta_t.Time = t;
% dtheta_t.Time = t;
%%% SHOULD NOT BE USED NOW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Update gain
kv1 = 0.0;
kt1 = 0.0;
kt2 = 0.0;
kv2 = 0.0;

% optimizationType denotes the type of optimization to be performed
% 0: only velocity optimization
% 1: only theta optimization
% 2: theta and velocity optimization
% 3: no optimization
switch optimizationType
    case 0
        kv2 = 2.0;
    case 1
        kt2 = 7.0;
    case 2
        if ~useMom00
            kv2 = 0.75;%2.0
            kt2 = 0.75;%7.0
        else
            kv2 = 0.1;%2.0
            kt2 = 1.0;%7.0
        end
    case 3
end

% Estimation gain
alpha  = [1 1 1];
% beta   = [5000 500 50];
% beta   = [2000 300 30];
if ~useMom00
    beta   = [1000 1000 1000];
else
    beta   = [500 500 500];
end
% beta   = [1000 1000 1000];
% beta   = [1 1 1];
% alpha  = 1e-1;
% beta   = 5e+2;
% P      = eye(n_m,n_m)   * alpha;
% Lambda = eye(n_nm,n_nm) * beta;
P      = diag(alpha);%eye(n_m,n_m)   * alpha;
Lambda = diag(beta);%eye(n_nm,n_nm) * beta;

useObserver = true;

% Set the initial condition
%chi0 = rand(3,1);
chi0 = [0.9;0.8;0.1];%chi0/norm(chi0);
% z0 = [0.5;-0.2;0.1]; % Non-measurable estimation initial error
% z0 = [0.1;-0.1;0.1]; % Non-measurable estimation initial error
% z0 = zeros(3,1); % Non-measurable estimation initial error
z0 = [0.5 1 -0.5]; % Non-measurable estimation initial error
v0 = zeros(3,1);
if ~useMom00
    v0(1) = -0.05;
    v0(2) = -0.05;
    v0(3) = -0.05;
else
    v0(1) = -0.05;
    v0(2) = -0.05;
    v0(3) = -0.05;
end% v0(1) = -0.0;
% v0(2) = -0.0;
% v0(3) = -0.15;
ki_vdes = 0.5*(v0'*v0);

% Selection matrix for the measurement model 
m_init = load('m_init');
m_init = m_init.m;

% Control gain
% Ks = diag([1.0;1.0;0.0005]); %with ground truth
if ~useMom00
    Ks = diag([0.5;0.5;0.5]); %with high overshoot
else
    Ks = diag([0.5;0.5;0.5]); %with high overshoot
end

% Tolerance value for d parameter initialization
%dtol = 1e-3;

% Tolerance value for vectors norm
tol = 1e-40;

% Maximum angular velocity norm
wmax = 0.013;
% wmax = 0.01;

% High and low thresholds of non measurable error to tune the control gain
% Ks
zmin = 0.01;
zmax = 60;
Ksmin = 0.01;
Ksmax = 1.0;

% Cut-off frequency for lowpass filter
fcut = 1;
tau = 1/fcut;
alpha = 1 / ( 1 + (tau/Ts));


amax = 0.25;
tdotmax = 0.25;

load('w_cmd.mat');