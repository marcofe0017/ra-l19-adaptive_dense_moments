close all
clear all
clc

% Flags
vFlag = true;
tFlag = false;
vtFlag = true;

% Select the folder from which load the data to plot
% optimizationType denotes the type of optimization to be performed
% 0: only velocity optimization
% 1: only theta optimization
% 2: theta and velocity optimization
% 3: no optimization
optimizationType = 2;

simulationNum = 0;
resFolderName = strcat('./sim',int2str(simulationNum),'_data/');
switch optimizationType
    case 0
        optTypeName = 'vOpt';
    case 1
        optTypeName = 'thetaOpt';
    case 2
        optTypeName = 'v-thetaOpt';
    case 3
        optTypeName = 'noOpt';
end
resFolderName = strcat(resFolderName,optTypeName,'/');
tOptFolderName = strcat(resFolderName,'thetaOpt','/');

% Load the dataset
% Current dataset
load(strcat(resFolderName,'results_',optTypeName,'.mat'));

% Theta-optimized dataset
if tFlag
    tOptRes = load(strcat(resFolderName,'../thetaOpt/results_thetaOpt','.mat'));
end

% Theta-optimized dataset
if vFlag
    vOptRes = load(strcat(resFolderName,'../vOpt/results_vOpt','.mat'));
end

% Theta-optimized dataset
if vtFlag
    vtOptRes = load(strcat(resFolderName,'../v-thetaOpt/results_v-thetaOpt','.mat'));
end



%% Live rho
% Plot determinant ro of Omega*OmegaT
h = figure;
grid on; box on; hold on;
set(gca,'gridlinestyle','--'); hold on;
title('Determinant \rho'); hold on;
N = size(ro.Time,1);
for i = 1 : N
    clf(h);
    grid on; box on; hold on;
    plot(ro.Time(1:i),ro.Data(1:i),'LineWidth',2);hold on;
    xlabel('Time [s]');
    ylabel('\rho');
    axis([0 14 0 1.8e-4]);
    legend('\rho');
    drawnow;
end

%% xi error
h = figure;
title('\xi error'); hold on;
N = size(xi_error.Time,1);
for i = 1 : N
    clf(h);
    grid on; box on; hold on;
    plot(xi_error.Time(1:i),xi_error.Data(1:i,:),'LineWidth',2);hold on;
    xlabel('Time [s]');
    ylabel('\xi');
    axis([0 14 -0.025 0.015]);
    legend('m_{w,1}','m_{w,2}','m_{w,3}');
    drawnow;
end

%% chi comparison / chi error
% chi comparison
chiNames = ['A';'B';'C'];
chiGTNames = strcat(chiNames,'_{GT}');
h = figure;
N = size(chi_comparison.Time,1);
xmin = [0 0 0];
xmax = [14 14 14];
ymin = [-0.6 -1 0.1];
ymax = [0.1 0.2 0.7];

for j = 1 : N
    clf(h);
    for i = 1 : 3
        legStr = {};
        subplot(3,1,i);
        grid on; box on; hold on;
        gtPlotNamei = chiGTNames(i,:);
        plotNamei   = chiNames(i,:);
        plot(chi_comparison.Time(1:j),chi_comparison.Data(2*i-1,1:j),'LineWidth',2);hold on;legStr = [legStr;gtPlotNamei];
        plot(chi_comparison.Time(1:j),chi_comparison.Data(2*i,1:j),'LineWidth',2);hold on;legStr = [legStr;plotNamei];
        xlabel('Time [s]');
        ylabel(strcat('\chi_',chiNames(i)));
        axis([xmin(i) xmax(i) ymin(i) ymax(i)]);
        legend(legStr);
    end
    drawnow;
end


%% z error
h = figure;
N = size(z_error.Time,1);
for i = 1 : N
    clf(h);
    grid on; box on; hold on;
    title('z error'); hold on;
    plot(z_error.Time(1:i),z_error.Data(1:i,:),'LineWidth',2);hold on;
    xlabel('Time [s]');
    ylabel('z');
    legend('z_{A}','z_{B}','z_{C}');
    axis([0 14 -0.6 1.2]);
    drawnow;
end
