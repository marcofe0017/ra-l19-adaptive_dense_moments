function [H,D1,sigmas] = computeH2(Omega,Lambda,P)

    % Compute OmegaTilde
    OmegaTilde = sqrt(Lambda) * Omega * sqrt(P);
    
    % Compute the SVD decomposition to get the diagonal matrix S and the
    % vector V
    [U,S,V] = svd(Omega);
    sigmas = eig(Omega);
    SS2 = real(S*S');
    V = real(V);
    % Compute Htilde
    alpha = P(1);
    beta  = Lambda(1);
%     D1 = 2*sqrt(S*S');
    D1 = 2*sqrt(SS2*alpha*beta);
%     D1 = 2*sqrt(S*S')/(alpha*beta);
%     Htilde = V * D1 * V';
    H = V * D1 * V';
    
    % Compute H
%     H = inv(sqrt(P)) * Htilde * sqrt(P);
end