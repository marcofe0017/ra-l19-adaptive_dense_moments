function VREPreader18v2(block)
%MSFUNTMPL_BASIC A Template for a Level-2 MATLAB S-Function
%   The MATLAB S-function is written as a MATLAB function with the
%   same name as the S-function. Replace 'msfuntmpl_basic' with the 
%   name of your S-function.
%
%   It should be noted that the MATLAB S-function is very similar
%   to Level-2 C-Mex S-functions. You should be able to get more
%   information for each of the block methods by referring to the
%   documentation for C-Mex S-functions.
%
%   Copyright 2003-2010 The MathWorks, Inc.

%%
%% The setup method is used to set up the basic attributes of the
%% S-function such as ports, parameters, etc. Do not add any other
%% calls to the main body of the function.
%%
setup(block);

%endfunction

%% Function: setup ===================================================
%% Abstract:
%%   Set up the basic characteristics of the S-function block such as:
%%   - Input ports
%%   - Output ports
%%   - Dialog parameters
%%   - Options
%%
%%   Required         : Yes
%%   C-Mex counterpart: mdlInitializeSizes
%%
function setup(block)

% Register number of ports
block.NumInputPorts  = 4;
block.NumOutputPorts = 5;

% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

% Override input port properties

% camera handle
block.InputPort(1).Dimensions        = 1;
block.InputPort(1).DatatypeID        = 6; % int32
block.InputPort(1).Complexity        = 'Real';
block.InputPort(1).DirectFeedthrough = true;
block.InputPort(1).SamplingMode      = 0;

% marker handle
block.InputPort(2).Dimensions        = 1;
block.InputPort(2).DatatypeID        = 6; % int32
block.InputPort(2).Complexity        = 'Real';
block.InputPort(2).DirectFeedthrough = true;
block.InputPort(2).SamplingMode      = 0;

% clock in input
block.InputPort(3).Dimensions        = 1;
block.InputPort(3).DatatypeID        = 0; % double
block.InputPort(3).Complexity        = 'Real';
block.InputPort(3).DirectFeedthrough = true;
block.InputPort(3).SamplingMode      = 0;

% start flag in input
block.InputPort(4).Dimensions        = 1;
block.InputPort(4).DatatypeID        = 8; % boolean
block.InputPort(4).Complexity        = 'Real';
block.InputPort(4).SamplingMode      = 0;

% Override output ep.port properties

% Homogeneous Transformation matrix camera-marker
block.OutputPort(1).Dimensions       = [4 4];
block.OutputPort(1).DatatypeID       = 0; % double
block.OutputPort(1).Complexity       = 'Real';
block.OutputPort(1).SamplingMode     = 0;

% camera image
block.OutputPort(2).Dimensions       = [240 320];
% block.OutputPort(2).Dimensions       = [480 640];
block.OutputPort(2).DatatypeID       =  3; %/* uint8_T   */
block.OutputPort(2).Complexity       = 'Real';
block.OutputPort(2).SamplingMode     = 0;

% Framework enable flag
block.OutputPort(3).Dimensions       = 1;
block.OutputPort(3).DatatypeID       = 8; % boolean
block.OutputPort(3).Complexity       = 'Real';
block.OutputPort(3).SamplingMode     = 0;

% Camera linear and angular velocity vector
block.OutputPort(4).Dimensions       = [6 1];
block.OutputPort(4).DatatypeID       = 0; %/* double_T   */
block.OutputPort(4).Complexity       = 'Real';
block.OutputPort(4).SamplingMode     = 0;

% Utility variable
block.OutputPort(5).Dimensions       = 1;
block.OutputPort(5).DatatypeID       =  0; %/* uint8_T   */
block.OutputPort(5).Complexity       = 'Real';
block.OutputPort(5).SamplingMode     = 0;



% Register parameters
block.NumDialogPrms     = 0;

% Register sample times
%  [0 offset]            : Continuous sample time
%  [positive_num offset] : Discrete sample time
%
%  [-1, 0]               : Inherited sample time
%  [-2, 0]               : Variable sample time
block.SampleTimes = [-1, 0];

% Specify the block simStateCompliance. The allowed values are:
%    'UnknownSimState', < The default setting; warn and assume DefaultSimState
%    'DefaultSimState', < Same sim state as a built-in block
%    'HasNoSimState',   < No sim state
%    'CustomSimState',  < Has GetSimState and SetSimState methods
%    'DisallowSimState' < Error out when saving or restoring the model sim state
block.SimStateCompliance = 'DefaultSimState';

%% -----------------------------------------------------------------
%% The MATLAB S-function uses an internal registry for all
%% block methods. You should register all relevant methods
%% (optional and required) as illustrated below. You may choose
%% any suitable name for the methods and implement these methods
%% as local functions within the same file. See comments
%% provided for each function for more information.
%% -----------------------------------------------------------------

% block.RegBlockMethod('PostPropagationSetup',    @DoPostPropSetup);
% block.RegBlockMethod('InitializeConditions', @InitializeConditions);
block.RegBlockMethod('Start', @Start);
block.RegBlockMethod('Outputs', @Outputs);     % Required
% block.RegBlockMethod('Update', @Update);
% block.RegBlockMethod('Derivatives', @Derivatives);
block.RegBlockMethod('Terminate', @Terminate); % Required

%end setup

%%
%% PostPropagationSetup:
%%   Functionality    : Setup work areas and state variables. Can
%%                      also register run-time methods here
%%   Required         : No
%%   C-Mex counterpart: mdlSetWorkWidths
%%
% function DoPostPropSetup(block)
% block.NumDworks = 1;
%   
%   block.Dwork(1).Name            = 'x1';
%   block.Dwork(1).Dimensions      = 1;
%   block.Dwork(1).DatatypeID      = 0;      % double
%   block.Dwork(1).Complexity      = 'Real'; % real
%   block.Dwork(1).UsedAsDiscState = true;


%%
%% InitializeConditions:
%%   Functionality    : Called at the start of simulation and if it is 
%%                      present in an enabled subsystem configured to reset 
%%                      states, it will be called when the enabled subsystem
%%                      restarts execution to reset the states.
%%   Required         : No
%%   C-MEX counterpart: mdlInitializeConditions
%%
% function InitializeConditions(block)

%end InitializeConditions


%%
%% Start:
%%   Functionality    : Called once at start of model execution. If you
%%                      have states that should be initialized once, this 
%%                      is the place to do it.
%%   Required         : No
%%   C-MEX counterpart: mdlStart
%
function Start(block)
    global vrep vrep_id offlineLog
    global B0remoteApi
    global image_raw
    global B0Tcr
    global B0vc
    global image_raw_prev
    global B0Tcr_prev
    global B0vc_prev
    global vcRet
    global cameraH refH
    
    rows = 240;
    cols = 320;
%     rows = 480;
%     cols = 640;
    image_raw = uint8(zeros(rows,cols,3)*255);
    image_raw_prev = image_raw;
    B0Tcr      = eye(4,4);
    B0Tcr_prev = eye(4,4);
    B0vc = zeros(1,6);
    B0vc_prev = zeros(1,6);
    vcRet = -1;
    
    if ~offlineLog
%         cameraH = block.InputPort(1).Data;
%         refH = block.InputPort(2).Data;
        if ~B0remoteApi 
            % Read the orientation for the first time (streaming flag)
            [ret, quat]       = vrep.simxGetObjectQuaternion(vrep_id,refH,cameraH,vrep.simx_opmode_streaming);
            [ret, abg]        = vrep.simxGetObjectOrientation(vrep_id,cameraH,-1,vrep.simx_opmode_streaming);
            [ret, tcm]        = vrep.simxGetObjectPosition(vrep_id,refH,cameraH,vrep.simx_opmode_streaming);
            [ret, res, image] = vrep.simxGetVisionSensorImage2(vrep_id,cameraH,10,vrep.simx_opmode_streaming);
            [retv, vwc, wwc]  = vrep.simxGetObjectVelocity(vrep_id,cameraH,vrep.simx_opmode_streaming);
        else
%             data = vrep.simxEvaluateToInt('sim.handleflag_axis',vrep.simxServiceCall());
%             axisflag = int8(data{2});
            vrep.simxGetObjectMatrix(int8(refH),int8(cameraH),vrep.simxDefaultSubscriber(@getTcr_CB,1));
            vrep.simxGetVisionSensorImage(int8(cameraH),true,vrep.simxDefaultSubscriber(@getImage_CB,1));
            vrep.simxGetObjectVelocity(int8(cameraH),vrep.simxDefaultSubscriber(@getVc_CB,1));
            vrep.simxGetSimulationStepStarted(vrep.simxDefaultSubscriber(@simulationStepStarted_CB,1));
            vrep.simxGetSimulationStepDone(vrep.simxDefaultSubscriber(@simulationStepDone_CB,1));

        end
    end
        
%     block.Dwork(1).Data = 0;
% end
%end Start

%%
%% Outputs:
%%   Functionality    : Called to generate block outputs in
%%                      simulation step
%%   Required         : Yes
%%   C-MEX counterpart: mdlOutputs
%%
function Outputs(block)
    global vrep 
    global vrep_id 
    global image_raw
    global B0remoteApi
    global B0Tcr
    global B0vc
    global cameraH refH
    global doNextStep
    global vcRet
    global image_raw_prev
    global B0Tcr_prev
    global B0vc_prev
    global start
    
    rows = 240;
    cols = 320;
%     rows = 480;
%     cols = 640;

    %start = block.InputPort(4).Data;
    
    utilVar = -1;
    
    if ~B0remoteApi
%         % Trigger v-rep
%         vrep.simxSynchronousTrigger(vrep_id);
% 
        %After this call, the first simulation step is finished (Blocking function call)
%         vrep.simxGetPingTime(vrep_id); 
    end
    
%     if start
        % Use readable variables
%         cameraH      = block.InputPort(1).Data;
%         refH         = block.InputPort(2).Data;
        clock        = block.InputPort(3).Data;
        

        if ~B0remoteApi
%             if(clock <= 0.05)
%                 % Read the orientation for the first time (streaming flag)
%                 [ret, quat]      = vrep.simxGetObjectQuaternion(vrep_id,refH,cameraH,vrep.simx_opmode_streaming);
%                 [ret, tcr]       = vrep.simxGetObjectPosition(vrep_id,refH,cameraH,vrep.simx_opmode_streaming);
%                 [retv, vwc, wwc] = vrep.simxGetObjectVelocity(vrep_id,cameraH,vrep.simx_opmode_streaming);
% 
%                 % Read the image from the camera
%                 [ret_img, res, image_raw] = vrep.simxGetVisionSensorImage2(vrep_id,cameraH,10,vrep.simx_opmode_streaming);
%                 img = image_raw;
%                 img = flipdim(image_raw,2); 
%                 img = flipdim(img,1);    
%             else
                % Read the orientation for the next times
                [ret, quat]      = vrep.simxGetObjectQuaternion(vrep_id,refH,cameraH,vrep.simx_opmode_buffer);
                [ret, tcr]       = vrep.simxGetObjectPosition(vrep_id,refH,cameraH,vrep.simx_opmode_buffer);
                [retv, vwc, wwc] = vrep.simxGetObjectVelocity(vrep_id,cameraH,vrep.simx_opmode_buffer);

                % Read the image from the camera
    %             [ret_img, res, image_raw] = vrep.simxGetVisionSensorImage2(vrep_id,cameraH,10,vrep.simx_opmode_oneshot);
                [ret_img, res, image_raw] = vrep.simxGetVisionSensorImage2(vrep_id,cameraH,10,vrep.simx_opmode_buffer);
                img = image_raw;
                img = flipdim(image_raw,2);
                img = flipdim(img,1);   


                if(ret_img==vrep.simx_return_ok)
                    utilVar=vrep.simxGetLastCmdTime(vrep_id);
                else
                    ret_img;
                end

%             end
            
            % Build the output homogeneous transformation matrix
            qcr = [quat(4) quat(1) quat(2) quat(3)];
            Rcr = quat2rotm(qcr);
            Tcr = eye(4);
            Tcr(1:3,1:3) = Rcr;
            Tcr(1:3,4)   = tcr;
            
        else % if using B0-based remote APIs

%             vrep.simxSynchronousTrigger();

%             vrep.simxSpinOnce();

%             if doNextStep
                Tcr = B0Tcr;
                vwc = B0vc(1:3);
                wwc = B0vc(4:6);
                img = image_raw;
                utilVar = vcRet;
%                 doNextStep = false;
%             else
%                 Tcr = B0Tcr_prev;
%                 vwc = B0vc_prev(1:3);
%                 wwc = B0vc_prev(4:6);
%                 img = image_raw_prev;
%                 utilVar = vcRet;
%             end
            
        end



        % Set the homogeneous transformation matrix on the output port
        if(sum(isnan(Tcr)) > 0)
            block.OutputPort(1).Data = eye(4);
            block.OutputPort(2).Data = uint8(zeros(rows,cols)*255);
            block.OutputPort(3).Data = false;
            block.OutputPort(4).Data = zeros(6,1);
        else                
            block.OutputPort(1).Data = double(Tcr);
            Rcw = double(Tcr(1:3,1:3));
            vcc = Rcw * double(vwc)';
            wcc = Rcw * double(wwc)';
%             vcc = double(vwc)';
%             wcc = double(wwc)';
            block.OutputPort(4).Data = [vcc;wcc];
        end
        block.OutputPort(5).Data = double(utilVar);

        % If not initialized yet, initialize the first image and set the
        % corresponding flag first_img

        if(sum(sum(img(:,:,1))) > 0)
            if ~B0remoteApi
                grayImg = rgb2gray(img); 
            else
%                 grayImg = rgb2gray(img); 
                grayImg = img;
            end
            block.OutputPort(2).Data = grayImg;
            block.OutputPort(3).Data = true;
        end
%     else
%             block.OutputPort(1).Data = eye(4);
%             block.OutputPort(2).Data = uint8(zeros(rows,cols)*255);%uint8(image(:,:,1));
%             block.OutputPort(3).Data = false;
%             block.OutputPort(4).Data = zeros(6,1);
%     end
    

%end Outputs

%%
%% Update:
%%   Functionality    : Called to update discrete states
%%                      during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlUpdate
%%
% function Update(block)
% 
% block.Dwork(1).Data = block.InputPort(1).Data;

%end Update

%%
%% Derivatives:
%%   Functionality    : Called to update derivatives of
%%                      continuous states during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlDerivatives
%%
% function Derivatives(block)

%end Derivatives

%%
%% Terminate:
%%   Functionality    : Called at the end of simulation for cleanup
%%   Required         : Yes
%%   C-MEX counterpart: mdlTerminate
%%
function Terminate(block)

% end Terminate

function getVc_CB(data)
    global B0vc;
    global B0vc_prev
    global vcRet;
    
    ret = data{1};
    vcRet = ret;
    
    if(ret == 1 && size(data,2)==3)
        B0vc_prev = B0vc;
        for i = 1 : 3
            B0vc(i) = cell2mat(data{2}(i));
            B0vc(i+3) = cell2mat(data{3}(i));
        end
%         B0vc(1:3) = cell2mat(data{2});
%         B0vc(4:6) = cell2mat(data{3});
    else
        B0vc = B0vc_prev;
    end
    
function getTcr_CB(data)
        global B0Tcr 
        global B0Tcr_prev

    ret = data{1};
    if(size(data,2)==2)
       mat = zeros(1,size(data{2},2));
       for i = 1 : size(data{2},2) 
           mat(i) = cell2mat(data{2}(i)); 
       end 
       %mat = cell2mat(data{2});
       B0Tcr_prev = B0Tcr;
       B0Tcr = [mat(1),  mat(2),  mat(3),  mat(4);
                mat(5),  mat(6),  mat(7),  mat(8);
                mat(9), mat(10), mat(11), mat(12);
                     0,       0,       0,       1;];
    else
        B0Tcr = B0Tcr_prev;
    end
   
function getImage_CB(data)
    global image_raw
    global image_raw_prev
    
    ret = data{1};
    
    if(size(data,2)==3)
        image_raw_prev = image_raw;
        cols = double(data{2}{1});
        rows = double(data{2}{2});
%         img = reshape(uint8(data{3}),rows,cols);
        img = reshape(uint8(data{3}),cols,rows)';
        img2 = flipdim(img,2); 
%         image_raw = flipdim(img2,1); 
        image_raw = img2;
    else
        image_raw = image_raw_prev;
    end
    
% end getImage_CB

function simulationStepStarted_CB(data)
    data=data{2};
    simTime=data('simulationTime');
%     disp(strcat('Simulation step started. Simulation time: ',num2str(simTime)));
%end simulationStepStarted_CB

function simulationStepDone_CB(data)
    global doNextStep;
    data=data{2};
    simTime=data('simulationTime');
%         disp(strcat('Simulation step done. Simulation time: ',num2str(simTime)));
    doNextStep=true;
%end simulationStepDone_CB